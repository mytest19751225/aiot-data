package cc.iotkit.data.service;

import cc.iotkit.data.model.TbDeviceOtaDetail;
import com.baomidou.mybatisplus.extension.service.IService;

public interface DeviceOtaDetailService extends IService<TbDeviceOtaDetail> {
}
