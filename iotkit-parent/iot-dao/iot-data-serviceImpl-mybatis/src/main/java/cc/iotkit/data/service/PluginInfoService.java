package cc.iotkit.data.service;

import cc.iotkit.data.model.TbPluginInfo;
import com.baomidou.mybatisplus.extension.service.IService;

public interface PluginInfoService extends IService<TbPluginInfo> {
}
