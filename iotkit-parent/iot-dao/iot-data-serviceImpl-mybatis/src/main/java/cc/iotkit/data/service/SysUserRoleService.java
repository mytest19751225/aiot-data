package cc.iotkit.data.service;

import cc.iotkit.data.model.TbSysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SysUserRoleService extends IService<TbSysUserRole> {
}
